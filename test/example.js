function func1() {
	function func1_2() { }
	func1_2();
	return func1_2;
}

var func3;
var func2 = function() { };
func3 = function() { };
(function() { })();
var ret = (function() { })();

func1();

module.exports = func1();

module.exports();

if(true) {
	module.exports();
	
	function func4() { }
}

{
	var test = 'testing';
	(function() {
		var anothertest = 'more tests';
	})();
}