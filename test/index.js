var stackhit = require('../lib/index.js'),
	util = require('util');

var de = stackhit({
	structure: stackhit.StackStructure.Tree
});

process = de.bug(process, 'process');
console = de.bug(console, 'console');
Buffer = de.bug(Buffer, 'Buffer');

var test = {
	test1: function() {
		this.test2(this.example);
	},
	test2: function(callback) {
		process.stdout.write('testing\n');
	},
	example: 'asdf'
};

de.bug(test, 'test');
test.test1();

var bufTest = new Buffer('testing');
Buffer.isBuffer(bufTest);
console.log(bufTest.toString());

console.log(util.inspect(de.Stack, {
	showHidden: true,
	depth: null,
	colors: true
}));