#! /usr/bin/env node

'use strict';

var path = require('path'),
	stackhit = require('../lib/index.js'),
	esprima = require('esprima'),
	escodegen = require('escodegen'),
	argv = require('minimist')(process.argv.slice(2));

var filepath, rootpath;
if(argv._ && argv._.length) {
    filepath = path.resolve(process.cwd(), argv._[0]);
    rootpath = path.dirname(filepath);
}
else {
    process.exit();
}

var de = stackhit();
global[de.StackID] = de;
function implantBugs(script, identifier) {
    for(var i = 0; i < script.length; i++) {
        var innerBody;
        if((innerBody = script[i].body) || (innerBody = script[i].consequent)) {
            if(innerBody.type === 'BlockStatement') {
                implantBugs(innerBody.body, identifier);
            }
            else {
                implantBugs(innerBody, identifier);
            }
        }

        if(script[i].type === 'FunctionDeclaration') {
            var bugExpression = {
                type: 'ExpressionStatement',
                expression: {
                    type: 'AssignmentExpression',
                    operator: '=',
                    left: {
                        type: 'Identifier',
                        name: script[i].id.name
                    },
                    right: {
                        type: 'CallExpression',
                        callee: {
                            type: 'Identifier',
                            name: 'global[\'' + de.StackID + '\'].bug'
                        },
                        arguments: [ script[i].id, {
                            type: 'Literal',
                            value: identifier + '.' + script[i].id.name,
                            raw: '\'' + identifier + '.' + script[i].id.name + '\''
                        }
                        ]
                    }
                }
            };
            script.splice(++i, 0, bugExpression);
        }
        else if(script[i].type === 'ExpressionStatement') {
            if(script[i].expression.type === 'AssignmentExpression') {
                var bugId = identifier + '.';

                if(script[i].expression.right.type === 'Identifier') {
                    bugId += script[i].expression.right.name;
                }
                else if(script[i].expression.left.type === 'Identifier') {
                    bugId += script[i].expression.left.name;
                }
                else if(script[i].expression.left.type === 'MemberExpression') {
                    var currentLevel = script[i].expression.left;
                    var id = '';
                    for(;;) {
                        id = '.' + currentLevel.property.name + id;
                        if(currentLevel.object.type === 'MemberExpression') {
                            currentLevel = currentLevel.object;
                        }
                        else {
                            id = currentLevel.object.name + id;
                            break;
                        }
                    }
                    bugId += id;
                }
                else {
                    bugId += '(anonymous[ln ' + script[i].expression.loc.start.line + ', col ' + script[i].expression.loc.start.column + '])';
                }

                script[i].expression.right = {
                    type: 'CallExpression',
                    callee: {
                        type: 'Identifier',
                        name: 'global[\'' + de.StackID + '\'].bug'
                    },
                    arguments: [ script[i].expression.right, {
                        type: 'Literal',
                        value: bugId,
                        raw: '\'' + bugId + '\''
                    }
					]
                };
            }
            else if(script[i].expression.type === 'CallExpression') {
                if(script[i].expression.callee.type === 'FunctionExpression' && script[i].expression.callee.body) {
                    if(script[i].expression.callee.body.type === 'BlockStatement') {
                        implantBugs(script[i].expression.callee.body.body, identifier);
                    }
                }
            }
        }
        else if(script[i].type === 'VariableDeclaration') {
            for(var o = 0; o < script[i].declarations.length; o++) {
                if(script[i].declarations[o].init !== null) {
                    script[i].declarations[o].init = {
                        type: 'CallExpression',
                        callee: {
                            type: 'Identifier',
                            name: 'global[\'' + de.StackID + '\'].bug'
                        },
                        arguments: [ script[i].declarations[o].init, {
                            type: 'Literal',
                            value: identifier + '.' + script[i].declarations[o].id.name,
                            raw: '\'' + identifier + '.' + script[i].declarations[o].id.name + '\''
                        }
                        ]
                    };
                }
            }
        }
    }
}

(function() {
    var m = require('module');

    var _origCompile = m.prototype._compile;
    m.prototype._compile = function(content, filename) {
        content = content.replace(/^\#\!.*/, '');

        var parsed = esprima.parse(content, { loc: true,
 tolerant: true });
        implantBugs(parsed.body, '[' + path.relative(rootpath, filename) + ']');
        content = escodegen.generate(parsed);

        return _origCompile.apply(this, arguments);
    };

    var _origRequire = m.prototype.require;
    m.prototype.require = function(name) {
        return de.bug(_origRequire.apply(this, arguments), name);
    };
})();

function processExitHandler(exit) {
    console.log(de.Stack);

    if(exit) {
        process.exit();
    }
}

process.on('exit', processExitHandler.bind(null, false));
process.on('SIGINT', processExitHandler.bind(null, true));

require(filepath);