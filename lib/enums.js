'use strict';

module.exports = {
    HitType: {
        Function: 0x0,
        Set: 0x1,
        Get: 0x2,
        SetAccessor: 0x3,
        GetAccessor: 0x4
    },
    StackStructure: {
        Linear: 0x0,
        Tree: 0x1,
        Combine: 0x2
    }
};