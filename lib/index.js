'use strict';

var enums = require('./enums.js'),
	hrtime = process.hrtime, // Any API functions that will be used inside the stack tracers must be saved here before they are potentially bugged
	memoryUsage = process.memoryUsage,
	createHash = require('crypto').createHash,
	now = Date.now;

var defaultOptions = {
    tree: enums.StackStructure.Linear
};

function startProfileCapture() {
    return {
        memory: memoryUsage().heapUsed,
        started: hrtime()
    };
}

function endProfileCapture(capture) {
    capture.memory = memoryUsage().heapUsed - capture.memory;
    var time = hrtime(capture.started);
    capture.time = time[0] * 1e9 + time[1];
    time = capture.started;
    capture.started = time[0] * 1e9 + time[1];
}

function StackHit(options) {
    this.StackID = createHash('md5').update(now().toString()).digest('hex');
    this.setOptions(options);
}

StackHit.prototype.setOptions = function(options, merge) {
    this._options = options = (function recurse(opts, def) {
        if(opts == null) {
            opts = def;
        }
        else {
            if(typeof def === 'object') {
                for(var entry in def) {
                    if(def.hasOwnProperty(entry)) {
                        opts[entry] = recurse(opts[entry], def[entry]);
                    }
                }
            }
        }
        return opts;
    })(options, merge && this._options ? this._options : defaultOptions);

    switch(options.structure) {
        default:
        case enums.StackStructure.Linear:
            this._hit_start = this._hit_start_linear;
            this._hit_end = this._hit_end_linear;
            this.Stack = [ ];
            this._stackIndex = 0;
            break;
        case enums.StackStructure.Tree:
            this._hit_start = this._hit_start_tree;
            this._hit_end = this._hit_end_tree;
            this.Stack = [ ];
            this._stackTree = [ ];
            break;
        case enums.StackStructure.Combine:
            this._hit_start = this._hit_start_combine;
            this._hit_end = this._hit_end_combine;
            this.Stack = { };
            break;
    }
};

StackHit.prototype._hit_start_linear = function(id, type) {
    this.Stack[this._stackIndex] = {
        id: id,
        type: type
    };
    return this._stackIndex++;
};

StackHit.prototype._hit_end_linear = function(index, profile, error) {
    var stack = this.Stack[index];
    stack.memory = profile.memory;
    stack.time = profile.time;
    stack.started = profile.started;
    stack.error = error;
};

StackHit.prototype._hit_start_combine = function(id, type) {
    if(this.Stack[id] == null) {
        this.Stack[id] = {
            id: id,
            type: type,
            memory: 0,
            time: 0,
            started: [ ],
            errors: [ ]
        };
    }
    return id;
};

StackHit.prototype._hit_end_combine = function(index, profile, error) {
    var stack = this.Stack[index];
    stack.memory += profile.memory;
    stack.time += profile.time;
    stack.started.push(profile.started);
    if(error) {
        stack.errors.push(error);
    }
};

StackHit.prototype._hit_start_tree = function(id, type) {
    var current = {
        id: id,
        type: type,
        memory: 0,
        time: 0,
        started: 0,
        error: undefined,
        stack: [ ]
    };

    var len = this._stackTree.length;
    if(len === 0) {
        this.Stack.push(current);
    }
    else {
        this._stackTree[len - 1].stack.push(current);
    }

    this._stackTree.push(current);
};

StackHit.prototype._hit_end_tree = function(index, profile, error) {
    var current = this._stackTree[this._stackTree.length - 1];
    this._stackTree.pop();
    current.memory = profile.memory;
    current.time = profile.time;
    current.started = profile.started;
    current.error = error;
};

function bugVariable(prop, v, i, id, target, self) {
    Object.defineProperty(target, i, {
        get: function() {
            var ret;

            var index = self._hit_start(id + '[' + i + ']', enums.HitType.Get),
            profile = startProfileCapture();
            ret = v;
            endProfileCapture(profile);
            self._hit_end(index, profile);

            return ret;
        },
        configurable: prop.configurable,
        enumerable: prop.enumerable
    });
    if(prop.writable) {
        Object.defineProperty(target, i, {
            set: function(value) {
                var index = self._hit_start(id + '[' + i + ']', enums.HitType.Set),
                profile = startProfileCapture();
                v = value;
                endProfileCapture(profile);
                self._hit_end(index, profile);
            }
        });
    }
}

function bugAccessor(i, id, target, prop, self) {
    if(prop.set) {
        Object.defineProperty(target, i, {
            set: function(value) {
                var error;

                var index = self._hit_start(id + '[' + i + '][set]', enums.HitType.SetAccessor),
                profile = startProfileCapture();
                try {
                    prop.set(value);
                }
                catch(ex) {
                    error = ex;
                }
                endProfileCapture(profile);
                self._hit_end(index, profile, error);

                if(error) {
                    throw error;
                }
            }
        });
    }
    if(prop.get) {
        Object.defineProperty(target, i, {
            get: function() {
                var ret, error;

                var index = self._hit_start(id + '[' + i + '][get]', enums.HitType.GetAccessor),
                profile = startProfileCapture();
                try {
                    ret = prop.get();
                }
                catch(ex) {
                    error = ex;
                }
                endProfileCapture(profile);
                self._hit_end(index, profile, error);

                if(error) {
                    throw error;
                }

                return ret;
            }
        });
    }
}

StackHit.prototype._crawl = function(obj, id, level, target) {
    for(var i in obj) {
        if(i !== this.StackID && obj.hasOwnProperty(i) && obj[i] != null) {
            if(target == null) {
                target = obj;
            }

            if(typeof obj[i] === 'function') {
                target[i] = this.bug(obj[i], id + '[' + i + ']', level + 1);
            }
            else if(!obj[i][this.StackID]) {
                if(Object.isExtensible(obj[i])) {
                    obj[i][this.StackID] = true;
                }
                var prop = Object.getOwnPropertyDescriptor(obj, i);
                if(!prop || (!prop.get && !prop.set)) {
                    bugVariable(prop, obj[i], i, id, target, this);
                }
                else if(prop.configurable) {
                    bugAccessor(i, id, target, prop, this);
                }
            }
        }
    }
};

StackHit.prototype.bug = function(obj, id, level) {
    if(obj == null || Object.isFrozen(obj) || obj[this.StackID] || (this._options.maxLevels != null && level >= this._options.maxLevels)) {
        return obj;
    }

    if(!id && !(id = obj.name)) {
        id = obj.toString();
        id = id.substr(9, id.indexOf('(') - 9);
    }

    if(!id) {
        throw new Error('No debugging identifier could be discovered, one must be manually supplied');
    }

    if(!level) {
        level = 0;
    }

    if(Object.isExtensible(obj)) {
        obj[this.StackID] = true;
    }

    var ret;
    if(typeof obj === 'function') {
        this._crawl(obj.prototype, id + '.prototype', level);

        var self = this;
        ret = function() {
            var ret, error;

            var index = self._hit_start(id, enums.HitType.Function),
            profile = startProfileCapture();
            try {
                ret = obj.apply(this, arguments);
            }
            catch(ex) {
                error = ex;
            }
            endProfileCapture(profile);
            self._hit_end(index, profile, error);

            if(error) {
                throw error;
            }

            return ret;
        };
        ret.prototype = obj.prototype;
        if(ret.prototype) {
            ret.prototype.constructor = ret;
        }
    }
    else {
        ret = obj;
    }

    this._crawl(obj, id, level, ret);

    return ret;
};

module.exports = function(options) {
    return new StackHit(options);
};
module.exports.StackStructure = enums.StackStructure;
module.exports.HitType = enums.HitType;